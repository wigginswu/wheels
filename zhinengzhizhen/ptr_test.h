//
// Created by 11754 on 2020/8/25.
//
#ifndef WHEELS_PTR_TEST_H
#define WHEELS_PTR_TEST_H

#include <string>
#include <memory>
#include <iostream>
using namespace std;

void shared_ptr_test()
{
    string *s1 = new string("s1");

    shared_ptr<string> ps1(s1);
    shared_ptr<string> ps2;
    ps2 = ps1;

    cout << ps1.use_count()<<endl;	//2
    cout<<ps2.use_count()<<endl;	//2
    cout << ps1.unique()<<endl;	//0

    string *s3 = new string("s3");
    shared_ptr<string> ps3(s3);

    cout << (ps1.get()) << endl;
    cout << ps3.get() << endl;
    swap(ps1, ps3);	//交换所拥有的对象
    cout << (ps1.get())<<endl;
    cout << ps3.get() << endl;

    cout << ps1.use_count()<<endl;	//1
    cout << ps2.use_count() << endl;	//2
    ps2 = ps1;
    cout << ps1.use_count()<<endl;	//2
    cout << ps2.use_count() << endl;	//2
    ps1.reset();	//放弃ps1的拥有权，引用计数的减少
    cout << ps1.use_count()<<endl;	//0
    cout << ps2.use_count()<<endl;	//1
}

void unique_ptr_test()
{
    unique_ptr<string> pu1(new string ("hello world"));
    unique_ptr<string> pu2;
    // pu2 = pu1;                                      // #1 不允许
    unique_ptr<string> pu3;
    pu3 = unique_ptr<string>(new string ("You"));   // #2 允许
}


class B;
class A
{
public:
    //shared_ptr<B> pb_; // shared_ptr循环引用
    weak_ptr<B> pb_;
    ~A()
    {
        cout<<"A delete\n";
    }
};

class B
{
public:
    shared_ptr<A> pa_;
    ~B()
    {
        cout<<"B delete\n";
    }
};

void shared_ptr_test_2()
{
    shared_ptr<B> pb(new B());
    shared_ptr<A> pa(new A());
    cout << pb.use_count() << endl;
    cout << pa.use_count() << endl;
    pb->pa_ = pa;
    pa->pb_ = pb;
    cout << pb.use_count() << endl;
    cout << pa.use_count() << endl;

}
#endif //WHEELS_PTR_TEST_H
