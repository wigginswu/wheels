//
// Created by 11754 on 2020/8/25.
// 智能指针实现
//

#ifndef WHEELS_PTRIMPL_H
#define WHEELS_PTRIMPL_H
#include <iostream>
using namespace std;
class Counter
{
public:
    Counter():s(0),w(0){};
    int s;
    int w;
};

template <class T>
class WeakPtr;

template <class T>
class SharedPtr
{
public:
    SharedPtr(T *p = 0):_ptr(p)
    {
        cnt = new Counter();
        if(p)
            cnt->s = 1;
        cout<<"in constructor "<<cnt->s<<endl;
    }

    ~SharedPtr()
    {
        release();
    }

    SharedPtr(SharedPtr<T> &s)
    {
        cout<<"in copy con "<<endl;
        _ptr = s._ptr;
        (s.cnt)->s++;
        cout<<"copy construct "<<(s.cnt)->s<<endl;
        cnt = s.cnt;
    }

    SharedPtr(WeakPtr<T> const &w)
    {
        cout<< "in w copy con "<<endl;
        _ptr = w.ptr;
        (w.cnt)->s++;
        cout<<"copy w constructor "<<(w.cnt)->s<<endl;
    }

    SharedPtr<T> &operator=(SharedPtr<T> &s)
    {
        if(this != &s)
        {
            release();
            (s.cnt)->s++;
            cout<<"assign construct "<<(s.cnt)->s<<end;
            cnt = s.cnt;
            _ptr = s._ptr;
        }
        return *this;
    }

    T &operator*()
    {
        return *_ptr;
    }

    T *operator->()
    {
        return _ptr;
    }

    friend class WeakPtr<T>;
protected:
    void release() {
        cnt->s--;
        cout << "release " << cnt->s << endl;
        if (cnt->s < 1) {
            delete _ptr;
            if(cnt->w < 1){
                delete cnt;
                cnt = NULL;
            }
        }
    }
private:
    T *_ptr;
    Counter * cnt;
};

template <class T>
class WeakPtr
{
public:
    WeakPtr()
    {
        _ptr = 0;
        cnt = 0;
    }

    WeakPtr(SharedPtr<T> &s):_ptr(s.ptr),cnt(s.cnt)
    {
        cout<<"w con s"<<endl;
        cnt->w++;
    }

    WeakPtr(WeakPtr<T> &w):_ptr(w.ptr),cnt(w.cnt){
        cnt->w++;
    }

    ~WeakPtr()
    {
        release();
    }

    WeakPtr<T> &operator=(WeakPtr<T> &w)
    {
        if (this != &w)
        {
            release();
            cnt = w.cnt;
            cnt->w++;
            _ptr = w._ptr;
        }
        return *this;
    }

    WeakPtr<T> &operator=(SharedPtr<T> &s)
    {
        cout << "w = s" << endl;
        release();
        cnt = s.cnt;
        cnt->w++;
        _ptr = s._ptr;
        return *this;
    }

    SharedPtr<T> lock()
    {
        return SharePtr<T>(*this);
    }

    bool expired()
    {
        if (cnt)
        {
            if (cnt->s > 0)
            {
                cout << "empty" << cnt->s << endl;
                return false;
            }
        }
        return true;
    }

protected:
    void release()
    {
        if(cnt)
        {
            cnt->w--;
            cout<<"weakptr release "<<cnt->w<<endl;
            if(cnt->w < 1 && cnt->s <1)
            {
                cnt = NULL;
            }
        }
    }
private:
    T *_ptr;
    Counter *cnt;
};

#endif //WHEELS_PTRIMPL_H
