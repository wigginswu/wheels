#pragma once
//
// Created by 11754 on 2020/8/27.
//
#include <string.h>
#include <iostream>
class stringImpl
{
public:
    // 默认构造函数
    stringImpl():data_(new char[1])
    {
        *data_ = '\0';
    }
    // 赋值构造函数
    stringImpl(const char* str):data_(new char[strlen(str)+1])
    {
        strcpy(data_,str);
    }

    // 拷贝构造函数
    stringImpl(const stringImpl& s)
    {
        if(&s != this)
        {
            data_ = new char[s.size() + 1];
            strcpy(data_,s.c_str());
        }
    }

    // 重载赋值运算符
    stringImpl& operator=(const stringImpl& s)
    {
        if(this == &s)
            return *this;
        delete [] data_;
        data_ = nullptr;
        data_ = new char [s.size() + 1];
        strcpy(data_,s.c_str());
        return *this;
    }

    ~stringImpl()
    {
        delete [] data_;
    }

    size_t size() const
    {
        return strlen(data_);
    }

    char* c_str() const
    {
        return data_;
    }
private:
    char*  data_;
};

void test_stringImpl()
{
    stringImpl s1 = "abc";
    std::cout<<s1.c_str()<<std::endl;

}