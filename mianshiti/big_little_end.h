//
// Created by 11754 on 2020/8/28.
// 判断系统大小端
//
#ifndef WHEELS_BIG_LITTLE_END_H
#define WHEELS_BIG_LITTLE_END_H

#include <iostream>
using namespace std;
void test_endian()
{
    union endian
    {
        int a;
        char b;
    }en;

    en.a = 1;
    if(en.b == 1)
    {
        cout<<"little endian"<<endl;
    }
    else
    {
        cout<<"big endian"<<endl;
    }

}

#endif //WHEELS_BIG_LITTLE_END_H
