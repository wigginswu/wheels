//
// Created by 11754 on 2020/9/7.
//

#ifndef WHEELS_CPP_MEMORY_MODEL_H
#define WHEELS_CPP_MEMORY_MODEL_H

#include <iostream>
using namespace std;
class A {
public:
    A(int v) {
        _v = v;
    }
    void Run() {
        std::cout << "Hello" << std::endl;
        // std::cout<< _v <<std::endl;
    }

    virtual void Run2() {
        std::cout<<"Hello 2"<<std::endl;
    }
private:
    int _v;
};

void test_1()
{
    A * p = NULL;
    p->Run();
    p->Run2();
}
#endif //WHEELS_CPP_MEMORY_MODEL_H
