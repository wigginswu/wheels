//
// Created by 11754 on 2020/11/2.
//
#include <iostream>
using namespace std;
#ifndef WHEELS_TYPE_TRANSFORM_H
#define WHEELS_TYPE_TRANSFORM_H
class A{

};

class B{
public:
    B(){

    }

    B(int a){
        cout<<"constructor call: cast int to type B"<<endl;
    }

    explicit B(bool flag){
        cout<<"constructor call: forbiden implict cast"<<endl;
    }

    operator A(){
        cout<<"cast to type A"<<endl;
    }
};

void test_type_transform(){
    B b = 1;
    B b0 = false;
    B b1;
    (A)b1;
}
#endif //WHEELS_TYPE_TRANSFORM_H
