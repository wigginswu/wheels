//
// Created by 11754 on 2020/9/3.
//
#include <mutex>
#include <memory>
using namespace std;
#ifndef WHEELS_SINGLETON_H
#define WHEELS_SINGLETON_H

// 懒汉式
class  singleton_1
{
public:
    static singleton_1* getInstance()
    {
        if(m_instance_ptr== nullptr){
            m_instance_ptr = new singleton_1();
        }
        return m_instance_ptr;
    }
    ~singleton_1(){}
private:
    singleton_1(){};
    singleton_1(singleton_1 &)=delete;
    singleton_1& operator=(const singleton_1&)=delete;
    static singleton_1* m_instance_ptr;
};
singleton_1* singleton_1::m_instance_ptr = nullptr;

// 懒汉式+线程安全（双检锁）+内存安全(智能指针)

class singleton_2
{
public:
    typedef shared_ptr<singleton_2> shptr;
    static shptr get_instance(){
        if(m_inst_ptr == nullptr)
        {
            mtx.lock();
            if(m_inst_ptr == nullptr){
                // 双检索存在失效的情况：m_inst_ptr = new singleton_3做了三件事：
                // 1.分配内存；2.构造初始化对象；3.将指针指向这片内存
                // 编译器转化时，步骤2,3可能发生对调，当A线程进入时检出指针为空，开辟内存，把指针指向该内存，随后再构造初始化对象
                // 另一个线程B此时进入第一个if判断指针不为空，返回指针给调用者，这时候对象还没有构造完成，就会造成错误
                m_inst_ptr = shptr (new singleton_2());
            }
            mtx.unlock();
        }
    }
    ~singleton_2(){}
private:
    singleton_2(){};
    singleton_2(singleton_2 &)=delete;
    singleton_2& operator=(const singleton_2&)=delete;
    static shptr m_inst_ptr;
    static mutex mtx;
};

// 懒汉式3：静态局部变量。如果当变量在初始化的时候，并发同时进入声明语句，并发线程将会阻塞等待初始化结束。
class singleton_3
{
public:
    ~singleton_3(){}
    singleton_3(const singleton_3&)=delete;
    singleton_3& operator=(const singleton_3&)=delete;
    static singleton_3& get_instance(){
        static singleton_3 instance;
        return instance;

    }
private:
    singleton_3(){}
};

// 饿汉式
class singleton_4
{
public:
    static singleton_4* getInstance()
    {
        return &m_inst;
    }
    ~singleton_4(){}
private:
    singleton_4(){}
    singleton_4(const singleton_4& s)=delete;
    singleton_4& operator=(const singleton_4& s)=delete;
    static singleton_4 m_inst;
};
#endif //WHEELS_SINGLETON_H
