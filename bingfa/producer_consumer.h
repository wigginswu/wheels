//
// Created by 11754 on 2020/8/26.
// 生产者消费者模型

#ifndef WHEELS_PRODUCER_CONSUMER_H
#define WHEELS_PRODUCER_CONSUMER_H

#include <iostream>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <queue>
#include <unistd.h>

std::mutex mtx;
std::condition_variable produce,consume;
std::queue<int> q;
int maxSize = 20;

void consumer()
{
    while(true)
    {
        sleep(0.8);
        std::unique_lock<std::mutex> lck(mtx);
        while(q.size() == 0)
        {
            consume.wait(lck);
        }

        std::cout<<"consumer "<<std::this_thread::get_id()<<":";
        q.pop();
        std::cout<<q.size()<<std::endl;

        produce.notify_all();
        lck.unlock();
    }
}

void producer(int id)
{
    while(true)
    {
        sleep(0.5);
        std::unique_lock<std::mutex> lck(mtx);
        while(q.size() == maxSize)
        {
            produce.wait(lck);
        }

        std::cout<<"-> producer "<<std::this_thread::get_id()<<": ";
        q.push(id);
        std::cout<<q.size()<<std::endl;

        consume.notify_all();
        lck.unlock();
    }
}

void test_consumer_producer()
{
    std::thread consumers[3], producers[2];
    for (int i = 0; i < 2; ++i)
    {
        producers[i] = std::thread(producer,i+1);
    }

    for (int i = 0; i < 3; ++i)
    {
        consumers[i] = std::thread(consumer);
    }

    for(int i=0;i<2;++i)
    {
        producers[i].join();
    }

    for(int i=0;i<3;++i)
    {
        consumers[i].join();
    }

    return;
}

#endif