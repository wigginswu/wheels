//
// Created by 11754 on 2020/8/26.
// 多线程顺序打印ABC
//

#ifndef WHEELS_PRINTABC_H
#define WHEELS_PRINTABC_H

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

std::mutex mtx;
std::condition_variable cv;
int num = 0;
void print_char(char c)
{
    int n = c - 'A';
    for(int i = 0;i<10;i++)
    {
        std::unique_lock<std::mutex> l(mtx);
        while (num != n) {
            cv.wait(l);
        }
        std::cout << "thread:" << std::this_thread::get_id() << " char:" << c << std::endl;
        num = (num + 1) % 3;
        l.unlock();
        cv.notify_all();
    }
}

void test_printABC()
{
    std::thread print_thread[3];
    for(int i = 0;i < 3;i++)
    {
        print_thread[i] = std::thread(print_char,'A'+i);
    }

    for(int i = 0;i < 3;i++)
    {
        print_thread[i].join();
    }
}
#endif //WHEELS_PRINTABC_H
