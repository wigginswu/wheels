# pragma once
//
// Created by 11754 on 2020/8/28.
// 实现LRU缓存机制
//
#include <unordered_map>
#include <list>
using namespace std;
class LRUCache {
private:
    int cap;
    list<pair<int,int>> l;
    unordered_map<int,list<pair<int,int>>::iterator> umap; // unordered_map内部实现hashtable，支持O(1)查找

public:
    LRUCache(int capacity) {
        cap = capacity;
    }

    int get(int key) {
        auto iter = umap.find(key);
        if(iter == umap.end())
            return -1;
        int val = iter->second->second;
        // 重新插入表头
        put(key,val);
        return val;
    }

    void put(int key, int value) {
        auto iter = umap.find(key);
        if(iter != umap.end())
        {
            l.erase(iter->second);
        }
        // 公共操作，list头部插入节点，更新umap
        l.push_front(make_pair(key,value));
        umap[key] = l.begin();
        if(l.size() > cap)
        {
            int last = l.back().first;
            umap.erase(last);
            l.pop_back();
        }

    }
};
