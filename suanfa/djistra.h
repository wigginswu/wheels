#pragma once
//
// Created by 11754 on 2020/9/3.
//
#include<vector>
#include <queue>
#include <algorithm>
#include<climits>
using namespace std;
/*
 * graph：邻接矩阵
 * start：起点
 * d[i]:起点到i的最短路径
 * p[i]:记录最短路径与i连接的前一个点
 */
void dji(vector<vector<int>> graph,int start,vector<int> &d,vector<int> &p){
    int v,min,w,k;
    int node_size = graph.size();
    vector<int> visited(node_size,0);

    // 初始化
    for(v = 0;v<node_size;v++){
        d[v] = graph[start][v];
        if(d[v] == INT_MAX)
            p[v] = -1;
        else
            p[v] = start;
    }
    d[start] = 0;
    p[start] = -1;
    visited[start] = 1;

    for(v = 1;v<node_size;v++){
        min = INT_MAX;
        // 找到当前未访问过的节点集合中与start最近的点
        for(w=0;w<node_size;w++){
            if(!visited[w] && min > d[w]){
                min = d[w];
                k = w;
            }
        }
        // 将该节点标记为已访问
        visited[k] = 1;
        // 更新最短路径
        for(w=0;w<node_size;w++){
            if(!visited[w] && d[w]>(min + graph[k][w])){
                d[w] = min +graph[k][w];
                p[w] = k;
            }
        }
    }
}

// 自定义排序
struct cmp{
    bool operator()(pair<int,int> p1,pair<int,int>p2){
        return p1.second < p2.second;
    }
};

// 用pair代表结点
// 堆优化
void dji2(vector<vector<int>> graph,int start,vector<int> &d,vector<int> &p){
    int v;
    int node_size = graph.size();
    // 访问数组
    vector<int> visited(node_size,0);
    // 小顶堆
    priority_queue<pair<int,int>,vector<pair<int,int>>,cmp> min_heap;
    // 初始化
    for(v=0;v<node_size;v++){
        d[v] = INT_MAX;
        p[v] = -1;
    }
    d[start] = 0;

    // 起始节点入堆
    min_heap.push(make_pair(start,d[start]));
    while(!min_heap.empty()){
        pair<int,int> node = min_heap.top();
        min_heap.pop();
        visited[node.first] = 1;
        for(int i=0;i<node_size;i++){
            if(!visited[i] && d[i] > node.second + graph[node.first][i]){
                d[i] = node.second + graph[node.first][i];
                p[i] = node.first;
                // 更新的节点入堆
                min_heap.push(make_pair(i,d[i]));
            }
        }
    }
}

// 搜索dji计算得到的最短路径
vector<int> search_path(int dest,vector<int> &p){
    vector<int> res;
    int k = dest;
    res.push_back(dest);
    while(k != -1){
        k = p[k];
        if(k != -1)
            res.push_back(k);
    }
    // 反转vector
    reverse(res.begin(),res.end());
    return res;
}