//
// Created by 11754 on 2020/9/8.
//
#include <iostream>
using namespace std;
#ifndef WHEELS_SORT_H
#define WHEELS_SORT_H
int partition(int arr[],int low,int high){
    int temp = arr[low];
    int i=high,j=low;
    while(j<i){
        while(j<i && arr[i] >= temp) i--;
        arr[j] = arr[i];
        while(j<i && arr[j] <= temp) j++;
        arr[i] = arr[j];
    }
    arr[j] = temp;
    return j;
}

void quicksort(int arr[],int low,int high){
    if(low < high){
        int idx = partition(arr,low,high);
        quicksort(arr,low,idx-1);
        quicksort(arr,idx+1,high);
    }
}

void test_sort(){
    int arr[] = { 49, 38, 65, 97, 23, 22, 76, 1, 5, 8, 2, 0, -1, 22 };
    int arr_len = 13;
    cout<<"origin arr:";
    for(int i=0;i<arr_len;i++)
        cout<<arr[i]<<' ';
    cout<<endl;

    quicksort(arr,0,arr_len-1);

    cout<<"sorted: ";
    for(int i=0;i<arr_len;i++)
        cout<<arr[i]<<' ';
}

#endif //WHEELS_SORT_H
